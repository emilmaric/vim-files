set expandtab			" change TABs into spaces
set autoindent			" do smart auto indentations
set smarttab			" smart tabbing
set shiftwidth=4		" sets tabbing for '>>' '<<' and '=='
set tabstop=4			" set number of spaces vim visually will show for TAB
set softtabstop=4		" sets how many spaces are shown for TAB key press
