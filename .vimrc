syntax on			        " enable syntax processing
filetype plugin indent on	" load filetype-specific indent files
set number			        " show line numbers
set ruler                   " show current column and line number in the status bar
set showcmd			        " show command in bottom bar
set wildmenu			    " visual autocomplete for command menu
set showmatch			    " highlight matching parantehsis/brackets/braces
set hlsearch			    " highlight words matched in a search pattern
let mapleader=","		    " set mapleader to be a comma

" Settings to turn on solarized colorscheme
set background=dark
colorscheme solarized

" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" easier split navigation
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>
